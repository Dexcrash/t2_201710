package test.data_structures;

import junit.framework.TestCase;
import model.data_structures.ListaEncadenada;

public class TestListaEncadenada extends TestCase {

	private ListaEncadenada<String> lista;

	public void setupEscenario1() {
		lista = new ListaEncadenada<String>();
	}

	public void setupEscenario2() {
		lista = new ListaEncadenada<String>();
		lista.agregarElementoFinal("Yo");
	}

	public void setupEscenario3() {
		lista = new ListaEncadenada<String>();
		lista.agregarElementoFinal("Yo");
		lista.agregarElementoFinal("Tu");
		lista.agregarElementoFinal("El");
		lista.agregarElementoFinal("Ella");
		lista.agregarElementoFinal("Ustedes");
		lista.agregarElementoFinal("Nosotros");
		lista.agregarElementoFinal("Eso");
		lista.agregarElementoFinal("Esa");
		lista.agregarElementoFinal("Vos");
		lista.agregarElementoFinal("Vosotros");
	}

	public void testAgregarAlFinal1() {
		setupEscenario1();
		assertNull("No deberia haber ningun objeto guardado.", lista.darElemento(0));
	}

	public void testAgregarAlFinal2() {
		setupEscenario2();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(lista.darNumeroElementos() - 1));
		lista.agregarElementoFinal("Tu");
		assertEquals("El elemento es incorrecto", "Tu", lista.darElemento(lista.darNumeroElementos() - 1));
		lista.agregarElementoFinal("Lo");
		assertEquals("El elemento es incorrecto", "Lo", lista.darElemento(lista.darNumeroElementos() - 1));
		lista.agregarElementoFinal("Uniandes");
		assertEquals("El elemento es incorrecto", "Uniandes", lista.darElemento(lista.darNumeroElementos() - 1));
	}

	public void testDarElemento1() {
		setupEscenario2();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
	}

	public void testDarElemento2() {
		setupEscenario3();
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
		assertEquals("El elemento es incorrecto", "El", lista.darElemento(2));
		assertEquals("El elemento es incorrecto", "Ustedes", lista.darElemento(4));
		assertEquals("El elemento es incorrecto", "Eso", lista.darElemento(6));
		assertEquals("El elemento es incorrecto", "Vos", lista.darElemento(8));
		assertEquals("El elemento es incorrecto", "Yo", lista.darElemento(0));
	}

	public void testDarNumeroElementos1() {
		setupEscenario2();
		assertEquals("El tama�o no es el adecuado", 1, lista.darNumeroElementos());
	}

	public void testDarNumeroElementos2() {
		setupEscenario3();
		assertEquals("El tama�o no es el adecuado", 10, lista.darNumeroElementos());
		lista.agregarElementoFinal("Uniandes");
		assertEquals("El tama�o no es el adecuado", 11, lista.darNumeroElementos());
		lista.agregarElementoFinal("LOL");
		assertEquals("El tama�o no es el adecuado", 12, lista.darNumeroElementos());
	}

	public void darElementoPosicionActual1() {
		setupEscenario2();
		assertEquals("El elemento no es correcto", "Yo", lista.darElementoPosicionActual());
	}

	public void darElementoPosicionActual2() {
		setupEscenario3();
		assertEquals("El elemento no es correcto", "Vosotros", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
		assertEquals("El elemento no es correcto", "Vos", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
		assertEquals("El elemento no es correcto", "Esa", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
		assertEquals("El elemento no es correcto", "Eso", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
		assertEquals("El elemento no es correcto", "Nosotros", lista.darElementoPosicionActual());
		lista.retrocederPosicionAnterior();
	}

	public void testAvanzarSiguientePosicion1() {
		setupEscenario2();
		assertFalse("No deberia poder avanzar", lista.avanzarSiguientePosicion());
	}

	public void testAvanzarSiguientePosicion2() {
		setupEscenario3();
		assertFalse("No deberia poder avanzar", lista.avanzarSiguientePosicion());
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		lista.retrocederPosicionAnterior();
		assertTrue("Deberia poder avanzar", lista.avanzarSiguientePosicion());
		assertTrue("Deberia poder avanzar", lista.avanzarSiguientePosicion());
		assertEquals("El elemento no es correcto", "Esa", lista.darElementoPosicionActual());
	}

	public void testRetrocederPosicionAnterior1() {
		setupEscenario2();
		assertFalse("No deberia poder retroceder", lista.retrocederPosicionAnterior());
	}

	public void testRetrocederPosicionAnterior2() {
		setupEscenario3();
		assertTrue("No deberia poder retroceder", lista.retrocederPosicionAnterior());
		assertTrue("No deberia poder retroceder", lista.retrocederPosicionAnterior());
		assertTrue("No deberia poder retroceder", lista.retrocederPosicionAnterior());
		assertTrue("No deberia poder retroceder", lista.retrocederPosicionAnterior());
		assertEquals("El elemento no es el correcto", "Nosotros", lista.darElementoPosicionActual());
	}

}
