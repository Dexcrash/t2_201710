package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaDobleEncadenada.NodoDoble;
import model.vo.VOPelicula;

public class ListaEncadenada<T> implements ILista<T> {

	
	NodoSencillo<T> first;
	NodoSencillo<T> current;
	int size;
	
	public ListaEncadenada() {
		// TODO Auto-generated constructor stub
		this.first = this.current = null;
		size=0;
	}
	
	public ListaEncadenada(T[] s){
		this.first = this.current = null;
		size=0;
		for(T si : s){
			agregarElementoFinal(si);
		}
	}
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new myIterator<>(first);
	}

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(first == null){
			first = current = new NodoSencillo<T>(null, elem);
			size = 1;
			return;
		}
		
		while(current.next!=null){
			current = current.next;
		}
		current.setNext(new NodoSencillo<T>(null, elem));
		current = current.next;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if(pos == 0 && first !=null)
			return first.getItem();
		if(pos<size && pos>0){
			current = first.getNext();
			pos--;
			while(pos>0){
				current = current.getNext();
				pos--;
			}
			return current.getItem();
		}
		return null;
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return current.getItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(current.next==null)
			return false;
		current = current.next;
		return true;
	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		NodoSencillo<T> temp;
		if(current.equals(first))
			return false;
		temp = first.getNext();
		while(!temp.getNext().equals(current)){
			temp = temp.getNext();
		}
		current = temp;
		return true;
	}

    private class myIterator<T> implements Iterator<T> {

        private NodoSencillo<T> current;

        public  myIterator(NodoSencillo<T> first) {
            current = first;
        }

        public boolean hasNext() {

            return current != null;
        }

        public T next() {
            if(hasNext()) {
                T item = current.item;
                current = current.next;
                return item;
            }
            return null;
        }
    }
	
	static class NodoSencillo<T>{
		NodoSencillo<T> next;
		T item;
		public NodoSencillo(NodoSencillo<T> next, T e) {
			// TODO Auto-generated constructor stub
			this.next = next;
			this.item = e;
		}
		public NodoSencillo<T> getNext() {
			return next;
		}
		public void setNext(NodoSencillo<T> next) {
			this.next = next;
		}
		public T getItem() {
			return item;
		}
		public void setItem(T item) {
			this.item = item;
		}
	}

	@Override
	public void set(T e, int i) {
		// TODO Auto-generated method stub
		if(i == 0 && first !=null)
			first.setItem(e);
		if(i<size && i>0){
			current = first.getNext();
			i--;
			while(i>0){
				current = current.getNext();
				i--;
			}
			current.setItem(e);
		}
		
	}
}
