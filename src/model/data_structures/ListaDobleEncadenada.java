package model.data_structures;

import java.util.Iterator;

import model.data_structures.ListaEncadenada.NodoSencillo;

public class ListaDobleEncadenada<T> implements ILista<T> {

	NodoDoble<T> first;
	NodoDoble<T> current;
	int size;
	
	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		return new myIterator<>(first);
	}
	

	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(first == null){
			first = current= new NodoDoble<T>(null,null, elem);
			size++;
			return;
		}
		
		while(current.next!=null)
			current = current.next;
		current.setNext(new NodoDoble<T>(null,current, elem));
		current = current.next;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if(pos == 0 && first !=null)
			return first.getItem();
		if(pos<size && pos>0){
			current = first.getNext();
			pos--;
			while(pos>0){
				current = current.getNext();
				pos--;
			}
			return current.getItem();
		}
		return null;
	}

	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		return current.getItem();
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(current.getNext()==null)
			return false;
		current = current.getNext();
		return true;
	}
	

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(current.getPrevious()==null)
			return false;
		current = current.getPrevious();
		return true;
	}
	
	public void set(T e ,int i){
		if(i == 0 && first !=null)
			first.setItem(e);
		if(i<size && i>0){
			current = first.getNext();
			i--;
			while(i>0){
				current = current.getNext();
				i--;
			}
			current.setItem(e);
		}
	}


    private class myIterator<T> implements Iterator<T> {

        private NodoDoble<T> current;

        public  myIterator(NodoDoble<T> first) {
            current = first;
        }

        public boolean hasNext() {

            return current != null;
        }

        public T next() {
            if(hasNext()) {
                T item = current.item;
                current = current.next;
                return item;
            }
            return null;
        }
    }
    
	static class NodoDoble<T>{
		NodoDoble<T> next;
		NodoDoble<T> previous;
		T item;
		public NodoDoble(NodoDoble<T> next,NodoDoble<T> previous, T e) {
			// TODO Auto-generated constructor stub
			this.next = next;
			this.previous = previous;
			this.item = e;
		}
		public NodoDoble<T> getNext() {
			return next;
		}
		public void setNext(NodoDoble<T> next) {
			this.next = next;
		}
		public T getItem() {
			return item;
		}
		public void setItem(T item) {
			this.item = item;
		}
		public NodoDoble<T> getPrevious() {
			return previous;
		}
		public void setPrevious(NodoDoble<T> previous) {
			this.previous = previous;
		}
		
	}
		
}
