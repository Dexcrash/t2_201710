package model.logic;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.omg.CORBA.portable.ValueOutputStream;

import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;

	private int agnoActual;

	public ManejadorPeliculas() {
		// TODO Auto-generated constructor stub
		misPeliculas = new ListaDobleEncadenada<>();
		peliculasAgno = new ListaDobleEncadenada<>();
		agnoActual = 1950;
	}

	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {
		// TODO Auto-generated method stub
		BufferedReader buff = null;
		String sepCvs = ",";
		String line = "";
		try {
			buff = new BufferedReader(new FileReader("./data/movies.csv"));
			line = buff.readLine();
			while ((line = buff.readLine()) != null) {
				String[] datos = line.split(sepCvs);
				String movieId = datos[0];
				String generos = datos[datos.length - 1];
				String nombre = "";
				for (int i = 1; i < datos.length - 1; i++) {
					nombre = nombre + datos[i];
				}

				if (nombre.startsWith("\"") && nombre.endsWith("\"")) {
					nombre = nombre.substring(1, nombre.length() - 1);
				}

				int a�o = -1;

				if (nombre.endsWith(")")) {
					Character g = nombre.charAt(nombre.length() - 2);
					if (g.isDigit(g)) {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 5, nombre.length() - 1));
					} else {
						a�o = Integer.parseInt(nombre.substring(nombre.length() - 6, nombre.length() - 2));
					}
				}

				nombre = nombre.substring(0, nombre.length() - 6);
				VOPelicula p = new VOPelicula();
				p.setAgnoPublicacion(a�o);
				p.setGenerosAsociados(new ListaEncadenada<>(generos.split("|")));
				p.setTitulo(nombre);
				misPeliculas.agregarElementoFinal(p);
			}

			System.out.println("--- Se cargaron las pelicula en : mis peliculas");
			peliculasAgno = peliculasA�o();
			System.out.println("--- Se cargaron las peliculas por a�o");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public ListaDobleEncadenada<VOPelicula> shellSort(ListaDobleEncadenada<VOPelicula> list) {
		int n = list.darNumeroElementos();
		int h = 1;
		while (h < (n / 3))
			h = 3 * h + 1;
		while (h >= 1) {
			for (int i = h; i < n; i++) {
				for (int j = i; j >= h && (list.darElemento(j).getAgnoPublicacion()
						- list.darElemento(j - h).getAgnoPublicacion() < 0); j -= h) {
					VOPelicula t = list.darElemento(j);
					list.set(list.darElemento(j - h), j);
					list.set(t, j - h);
				}
				h = h / 3;
			}
		}
		return list;
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPelicula> list = new ListaEncadenada<>();
		for (VOPelicula p : misPeliculas)
			if (p.getTitulo().contains(busqueda)) {
				list.agregarElementoFinal(p);
			}
		return list;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {
		for (VOAgnoPelicula ps : peliculasAgno) {
			for (VOPelicula p : ps.getPeliculas()) {
				System.out.println(p.getTitulo());
			}
		}

		int pos = agno - 1950;
		if (pos > 0 && pos < peliculasAgno.darNumeroElementos()) {
			return peliculasAgno.darElemento(pos).getPeliculas();
		} else {
			return null;
		}
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		// TODO Auto-generated method stub
		return peliculasAgno.avanzarSiguientePosicion() ? peliculasAgno.darElementoPosicionActual() : new VOAgnoPelicula();
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		// TODO Auto-generated method stub
		return peliculasAgno.retrocederPosicionAnterior() ? peliculasAgno.darElementoPosicionActual() : new VOAgnoPelicula();
	}

	public void printAll(ListaDobleEncadenada<VOPelicula> list) {
		for (VOPelicula p : list) {
			System.out.println(p.getTitulo());
		}
	}

	public ListaDobleEncadenada<VOAgnoPelicula> peliculasA�o() {
		
		ListaDobleEncadenada<VOAgnoPelicula> pelis = new ListaDobleEncadenada<>();
		for (int i = 1950; i < 2017; i++) {
			VOAgnoPelicula n = new VOAgnoPelicula();
			n.setAgno(i);
			ListaDobleEncadenada<VOPelicula> s = new ListaDobleEncadenada<>();
			for (VOPelicula p : misPeliculas) {
				if(p.getAgnoPublicacion()==i){
					s.agregarElementoFinal(p);
				}
			}
			n.setPeliculas(s);
			pelis.agregarElementoFinal(n);
		}

		return pelis;

	}

}
